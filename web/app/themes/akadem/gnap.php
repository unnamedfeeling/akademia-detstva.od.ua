<?php
/*
Template Name: Главное направление
*/
get_header(); ?>
			<img src="img/news_top.jpg" class="full-w" alt="">
			<section class="grey">
				<div class="container">
					<div class="row">
						<ul class="bred">
							<?= bcn_display_list(true) ?>
						</ul>
					</div>
				</div>
			</section>
			<section class="news_single">
				<img src="img/dec/team_dec1.png" alt="" class="news_dec news_dec-1">
				<img src="img/dec/team_dec2.png" alt="" class="news_dec news_dec-2">
				<div class="container">
					<div class="news_text">
						<div class="row va-middle">
							<div class="col-xs-12 col-sm-6">
								<img src="img/poster/p4.jpg" alt="">
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="news_name text-center">Заголовок</div>
								<p>
									С другой стороны рамки и место обучения кадров требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач. Значимость этих проблем настолько очевидна, что начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании новых предложений. Значимость этих проблем настолько очевидна, что укрепление и развитие структуры позволяет оценить значение модели развития. С другой стороны начало повседневной работы по формированию позиции требуют от нас анализа форм развития.
								</p>
							</div>
						</div>
						<div class="row va-middle">
							<div class="col-xs-12 col-sm-6">
								<div class="news_name text-center">Заголовок</div>
								<p>
									С другой стороны рамки и место обучения кадров требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач. Значимость этих проблем настолько очевидна, что начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании новых предложений. Значимость этих проблем настолько очевидна, что укрепление и развитие структуры позволяет оценить значение модели развития. С другой стороны начало повседневной работы по формированию позиции требуют от нас анализа форм развития.
								</p>
							</div>
							<div class="col-xs-12 col-sm-6">
								<img src="img/poster/p5.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="space" style="height: 15em"></div>
			</section>
			<section class="bluebg">
				<div class="bluebg_head unload " data-add="img/cut/btop.png" >   </div>
				<div class="posr">
					<div class="bluebg_cont">
						<section class="team">
							<div class="team_head unload " data-add="img/cut/btop.png" >   </div>
							<div class="posr">
								<div class="team_cont">
									<p class="h1">
										Наши преимущества
									</p>
									<div class="container ">
										<div class="row">
											<div class="col-xs-12 col-sm-3">
												<div class="tadv_el">
													<div class="tadv_top">
														<img src="img/squere.jpg" class="sample" alt="">
														<i class="icon-rocket"></i>
													</div>
													<div class="adv_text nodec">
														<p class="bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-3">
												<div class="tadv_el">
													<div class="tadv_top">
														<img src="img/squere.jpg" class="sample" alt="">
														<i class="icon-star"></i>
													</div>
													<div class="adv_text nodec">
														<p class="bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-3">
												<div class="tadv_el">
													<div class="tadv_top">
														<img src="img/squere.jpg" class="sample" alt="">
														<i class="icon-smile"></i>
													</div>
													<div class="adv_text nodec">
														<p class="bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-3">
												<div class="tadv_el">
													<div class="tadv_top">
														<img src="img/squere.jpg" class="sample" alt="">
														<i class="icon-lemon"></i>
													</div>
													<div class="adv_text nodec">
														<p class="bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="team_footer unload " data-add="img/cut/bbott.png" >   </div>
						</section>
					</div>
				</div>
				<div class="bluebg_footer unload " data-add="img/cut/bbott.png" >   </div>
			</section>
			<section class="gnap_team">
				<img src="img/dec/team_dec1.png" alt="" class="gnap_team-dec gnap_team-dec-1">
				<section class="team">
					<div class="posr">
						<div class="container team_cont">
							<div class="posr">
								<p class="h1">Наша команда</p>
								<div class="team_slider">
									<div class="team_slide">
										<div class="team_el">
											<div class="team_photo">
												<div class="team_img unload" data-dadd="img/photo/photo1.jpg">
													<img src="img/squere.jpg" class="sample" alt="">
												</div>
												<img src="img/dec/team_pdec.png" class="team_photo-decor">
											</div>
											<div class="team_text">
												<p class="team_name">Антонина Николаева</p>
												<p class="team_pos">Воспитатель</p>
											</div>
										</div>
									</div>
									<div class="team_slide">
										<div class="team_el">
											<div class="team_photo">
												<div class="team_img unload" data-dadd="img/photo/photo2.jpg">
													<img src="img/squere.jpg" class="sample" alt="">
												</div>
												<img src="img/dec/team_pdec.png" class="team_photo-decor">
											</div>
											<div class="team_text">
												<p class="team_name">Наталья Андреева</p>
												<p class="team_pos">Воспитатель</p>
											</div>
										</div>
									</div>
									<div class="team_slide">
										<div class="team_el">
											<div class="team_photo">
												<div class="team_img unload" data-dadd="img/photo/photo3.jpg">
													<img src="img/squere.jpg" class="sample" alt="">
												</div>
												<img src="img/dec/team_pdec.png" class="team_photo-decor">
											</div>
											<div class="team_text">
												<p class="team_name">Татьяна Иванова</p>
												<p class="team_pos">Воспитатель</p>
											</div>
										</div>
									</div>
									<div class="team_slide">
										<div class="team_el">
											<div class="team_photo">
												<div class="team_img unload" data-dadd="img/photo/photo4.jpg">
													<img src="img/squere.jpg" class="sample" alt="">
												</div>
												<img src="img/dec/team_pdec.png" class="team_photo-decor">
											</div>
											<div class="team_text">
												<p class="team_name">Маргарита Петрова</p>
												<p class="team_pos">Воспитатель</p>
											</div>
										</div>
									</div>

									<div class="team_slide">
										<div class="team_el">
											<div class="team_photo">
												<div class="team_img unload" data-dadd="img/photo/photo1.jpg">
													<img src="img/squere.jpg" class="sample" alt="">
												</div>
												<img src="img/dec/team_pdec.png" class="team_photo-decor">
											</div>
											<div class="team_text">
												<p class="team_name">Антонина Николаева</p>
												<p class="team_pos">Воспитатель</p>
											</div>
										</div>
									</div>
									<div class="team_slide">
										<div class="team_el">
											<div class="team_photo">
												<div class="team_img unload" data-dadd="img/photo/photo2.jpg">
													<img src="img/squere.jpg" class="sample" alt="">
												</div>
												<img src="img/dec/team_pdec.png" class="team_photo-decor">
											</div>
											<div class="team_text">
												<p class="team_name">Наталья Андреева</p>
												<p class="team_pos">Воспитатель</p>
											</div>
										</div>
									</div>
									<div class="team_slide">
										<div class="team_el">
											<div class="team_photo">
												<div class="team_img unload" data-dadd="img/photo/photo3.jpg">
													<img src="img/squere.jpg" class="sample" alt="">
												</div>
												<img src="img/dec/team_pdec.png" class="team_photo-decor">
											</div>
											<div class="team_text">
												<p class="team_name">Татьяна Иванова</p>
												<p class="team_pos">Воспитатель</p>
											</div>
										</div>
									</div>
									<div class="team_slide">
										<div class="team_el">
											<div class="team_photo">
												<div class="team_img unload" data-dadd="img/photo/photo4.jpg">
													<img src="img/squere.jpg" class="sample" alt="">
												</div>
												<img src="img/dec/team_pdec.png" class="team_photo-decor">
											</div>
											<div class="team_text">
												<p class="team_name">Маргарита Петрова</p>
												<p class="team_pos">Воспитатель</p>
											</div>
										</div>
									</div>
								</div>
								<div class="slider-dots text-right"></div>
							</div>
							<div class="slider-btn"></div>
						</div>
					</div>
				</section>
			</section>
			<section class="pinkblock">
				<div class="pinkblock_head ">  </div>
				<div class="pinkblock_cont">
					<div class="container">
						<div class="row va-middle full-w">
							<div class="col-xs-12 col-sm-9">
								<p class="pinkblock_h1">Мы всегда будем рады Вам и Вашим детям</p>
							</div>
							<div class="col-xs-12 col-sm-3 text-center">
								<button class="btn pinkblock_btn">
									подробнее
								</button>
							</div>
						</div>

					</div>
				</div>
				<div class="pinkblock_footer">  </div>
			</section>
			<section class="gnap_galery">
				<section class="galery">
					<div class="container">
						<p class="h1">
							Фотографии
						</p>
						<div class="posr">
							<div class="galery_slider">

								<div class="galery_slide">
									<div class="galery_el" onclick="galery(this)" data-large="img/photo/gal1.jpg">
										<img src="img/photo/gal1.jpg" alt="">
										<i class="icon-zoom-in"></i>
									</div>
								</div>
								<div class="galery_slide">
									<div class="galery_el" onclick="galery(this)">
										<img src="img/photo/gal2.jpg" data-large="img/photo/gal2.jpg" alt="">
										<i class="icon-zoom-in"></i>
									</div>
								</div>
								<div class="galery_slide">
									<div class="galery_el" onclick="galery(this)">
										<!-- no data-large example -->
										<img src="img/photo/gal3.jpg" alt="">
										<i class="icon-zoom-in"></i>
									</div>
								</div>
								<div class="galery_slide">
									<div class="galery_el" onclick="galery(this)" data-large="img/photo/gal1.jpg">
										<img src="img/photo/gal1.jpg" alt="">
										<i class="icon-zoom-in"></i>
									</div>
								</div>
								<div class="galery_slide">
									<div class="galery_el" onclick="galery(this)">
										<img src="img/photo/gal2.jpg" data-large="img/photo/gal2.jpg" alt="">
										<i class="icon-zoom-in"></i>
									</div>
								</div>
								<div class="galery_slide">
									<div class="galery_el" onclick="galery(this)" data-large="img/photo/gal3.jpg">
										<img src="img/photo/gal3.jpg" alt="">
										<i class="icon-zoom-in"></i>
									</div>
								</div>
							</div>
							<div class="slider-dots text-right"></div>
						</div>
						<div class="slider-btn"></div>
					</div>
					<div class="galery-popup">
						<div class="middler ">
							<img src="img/loader.gif" alt="">

						</div>
					</div>
				</section>
			</section>
			<section class="rev">
				<div class="rev_head ">  </div>
				<div class="rev_cont">
					<div class="container">
						<p class="h1">Отзывы</p>
					</div>
					<div class="posr">
						<div class="container">
							<div class="row">
								<div class="col-xs-12 col-xs-10 col-xs-offset-1">
									<div class="rev_slider">
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
										<div class="rev_slide">
											<div class="rev_el">
												<div class="rev_img">
													<img src="img/photo/rev_photo.jpg" alt="">
												</div>
												<div class="rev_info">
													<div class="rev_el-top">

														<div class="rev_nm">
															<div class="rev_name">Антонина Антонова</div>
															<div class="rev_pos">счастливая мама</div>
														</div>
														<div class="rev_date">17 января, 2018</div>
													</div>
													<div class="rev_text">
														<div class="rev_thider">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														</div>
													</div>
													<ul class="rev_bottom">
														<li>
															<i class="icon-share-square" ></i>
															<p>3</p>
														</li>
														<li>
															<i class="icon-heart-empty" ></i>
															<p>8</p>
														</li>

													</ul>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="slider-btn"></div>
					</div>
				</div>
				<div class="rev_footer">  </div>
			</section>
			<section class="bbl">
				<div class="bbl_img">
				</div>
				<div class="bbl_cont">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
								<p class="h1 bbl_h1">Lorem ipsum dolor sit amet</p>
								<div class="bbl_text">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								</div>
								<button class="btn bbl_btn">подробнее</button>
							</div>
						</div>


					</div>
				</div>
			</section>



	<!--
			<section class="test">
		<div class="head test"> <div class="test">head</div> </div>
		<div class="container">
			<div class="row">
				<div class="content test"> <div class="test"> cont</div> </div>
			</div>
		</div>
		<div class="footer test"> <div class="test">footer</div> </div>
	</section>

			<section>
				<div class="test_slider">
					<div class="test_slide">
						<img src="img/slide.jpg" alt="">
					</div>
					<div class="test_slide">
						<img src="img/slide.jpg" alt="">
					</div>
					<div class="test_slide">
						<img src="img/slide.jpg" alt="">
					</div>
					<div class="test_slide">
						<img src="img/slide.jpg" alt="">
					</div>
				</div>
			</section>
			<section class="test">
				<div class="test_head "> head </div>
				<div class="container">
					<div class="row">

					</div>
				</div>
				<div class="test_footer"> footer </div>
			</section>
		-->
<?php get_footer(); ?>
