<?php
get_header(); ?>
<img src="<?php echo get_field('bg_img_template')['url']; ?>" class="full-w" alt="<?php echo get_field('bg_img_template')['alt']; ?>">
<section class="grey">
	<div class="container">
		<div class="row">
			<ul class="bred">
				<?= bcn_display_list(true) ?>
			</ul>
		</div>
	</div>
</section>
<section class="news_single">
	<img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec1.png" alt="" class="news_dec news_dec-1">
	<img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec2.png" alt="" class="news_dec news_dec-2">
	<div class="container">
		<div class="news_text">

			<?php foreach( get_field('text_blocks_template') as $text_blocks_template ): ?>
            <div class="row va-middle">

              <?php if($text_blocks_template['position_img']=='left'): ?>
              <div class="col-xs-12 col-sm-6">
                <img src="<?php echo $text_blocks_template['img']['url']; ?>" alt="<?php echo $text_blocks_template['img']['alt']; ?>">
              </div>
              <?php endif; ?>

              <div class="col-xs-12 col-sm-6">
				<?php if($text_blocks_template['html_title_type'] == 'h1' && !empty($text_blocks_template['title'])):?>
					<h1 class="news_name text-center"><?php echo strip_tags($text_blocks_template['title']); ?></h1>
				<?php elseif($text_blocks_template['html_title_type'] == 'h2' && !empty($text_blocks_template['title'])): ?>
					<h2 class="news_name text-center"><?php echo strip_tags($text_blocks_template['title']); ?></h2>
				<?php elseif($text_blocks_template['html_title_type'] == 'h3' && !empty($text_blocks_template['title'])): ?>
					<h3 class="news_name text-center"><?php echo strip_tags($text_blocks_template['title']); ?></h3>
				<?php elseif(!empty($text_blocks_template['title'])): ?>
					<div class="news_name text-center"><?php echo strip_tags($text_blocks_template['title']); ?></div>
				<?php endif; ?>
                <p><?php echo $text_blocks_template['text']; ?></p>
              </div>

              <?php if($text_blocks_template['position_img']=='right'): ?>
              <div class="col-xs-12 col-sm-6">
                <img src="<?php echo $text_blocks_template['img']['url']; ?>" alt="<?php echo $text_blocks_template['img']['alt']; ?>">
              </div>
              <?php endif; ?>

            </div>
            <?php endforeach; ?>


		</div>
	</div>
	<div class="space" style="height: 15em"></div>
</section>

<?php if(get_field('show_block_feedback_template')): ?>
<section class="blueline">
	<div class="blueline_head ">  </div>
	<div class="blueline_cont">
		<div class="container">
			<div class="row va-middle full-w">
				<div class="col-xs-12 col-sm-9">
					<p class="pinkblock_h1"><?php the_field('text_feedback_template'); ?></p>
				</div>
				<div class="col-xs-12 col-sm-3 text-center">
					<button class="btn pinkblock_btn js-popup">
						<?php the_field('text_button_feedback_template'); ?>
					</button>
				</div>
			</div>

		</div>
	</div>
	<div class="blueline_footer">  </div>
</section>
<?php endif; ?>

<?php if(get_field('show_block_photo_template')): ?>
<div class="npb">
	<section class="galery">
		<div class="container">
			<p class="h1">
				<?php the_field('title_photo_template'); ?>
			</p>
			<div class="posr">
				<div class="galery_slider">

					<?php foreach( get_field('list_photo_template') as $list_photo_template ): ?>
	                <div class="galery_slide">
	                  <div class="galery_el" onclick="galery(this)" data-large="<?php echo $list_photo_template['full_img']['url']; ?>">
	                    <img src="<?php echo $list_photo_template['thumb']['url']; ?>" alt="<?php echo $list_photo_template['thumb']['url']; ?>">
	                    <i class="icon-zoom-in"></i>
	                  </div>
	                </div>
	                <?php endforeach; ?>

				</div>
				<div class="slider-dots text-right"></div>
			</div>
			<div class="slider-btn"></div>
		</div>
		<div class="galery-popup">
			<div class="middler ">
				<img src="<?= get_template_directory_uri(); ?>/img/loader.gif" alt="">

			</div>
		</div>
	</section>
</div>
<?php endif; ?>

<?php if(get_field('block_description_true')): ?>
<section class="wbl">

	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h3 class="h1"><?php the_field('title_block_description'); ?></h3>
					<div class="wbl_text">
						<?php the_field('desc_before_block_description'); ?>
						<div id="wblhidder" class="wbl_hidder">
							<?php the_field('desc_block_description'); ?>
						</div>
					</div>
				</div>
				<div class="text-center">
					<?php if(get_field('text_button_description')): ?>
						<button class="wbl_btn"><?php the_field('text_button_description'); ?></button>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

<div class="wbl_footer">   </div>
</section>
<?php endif; ?>

<section class="kkstarratings">
	<div class="container">
		<div class="col-xs-12"><?=do_shortcode( '[kkstarratings]' )?></div>
	</div>
</section>

<?php if(get_field('block_to_action_true')): ?>
<section class="bbl <?php echo get_field('show_map')? 'map_dec': ''?>">
	<div class="bbl_img bbl_img-2">
	</div>
	<div class="bbl_cont">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
					<p class="h1 bbl_h1"><?php the_field('title_block_to_action'); ?></p>
					<div class="bbl_text">
						<p><?php the_field('desc_block_to_action'); ?></p>
					</div>
					<?php if(get_field("variant_button")=="popup_form"): ?><button class="btn bbl_btn js-popup"><?php the_field('text_button_to_action'); ?></button>
					<?php elseif(get_field("variant_button")=="link"): ?><a href="<?php the_field('link_button_to_action'); ?>" class="btn bbl_btn"><?php the_field('text_button_to_action'); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="nap_footer">   </div>
</section>
<?php endif; ?>

<?php if(get_field('show_map')): ?>
<section class="map">
	<div class="gmap unload"></div>
</section>
<?php endif; ?>


<?php get_footer(); ?>
