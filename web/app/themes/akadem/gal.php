<?php
/*
Template Name: Галлерея
*/
get_header(); ?>
			<img src="<?= get_template_directory_uri(); ?>/img/gal_top.jpg" class="full-w" alt="">
			<section class="grey">
				<div class="container">
					<div class="row">
						<ul class="bred">
							<?= bcn_display_list(true) ?>
						</ul>
					</div>
				</div>
			</section>
			<section class="galery">
				<img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec1.png" alt="" class="news_dec news_dec-1">
				<img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec1.png" alt="" class="news_dec news_dec-2">
				<img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec1.png" alt="" class="galery_dec galery_dec-3">




				<div class="container">
					<p class="h1">Альбом</p>

					<div class="galery_top">
						<label for="sw1" class="active galery_btn" >
							Все
						</label>
						<label for="sw2" class="galery_btn" >
							Двор
						</label>
						<label for="sw3" class="galery_btn" >
							утренник
						</label>
					</div>
					<div class="switcher">
						<div class="switcher_el">
							<input type="radio" id="sw1" name="Switch1" checked="">
							<div class="switcher_cont">
								<div class="row">
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal1.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="switcher_el">
							<input type="radio" id="sw2" name="Switch1">
							<div class="switcher_cont">
								<div class="row">
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal2.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="switcher_el">
							<input type="radio" id="sw3" name="Switch1">
							<div class="switcher_cont">
								<div class="row">
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="galery_el" onclick="galery(this)" data-large="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg">
											<img src="<?= get_template_directory_uri(); ?>/img/photo/gal3.jpg" alt="">
											<i class="icon-zoom-in"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 text-center">
							<button class="galery_arrw">
								<img src="<?= get_template_directory_uri(); ?>/img/arw_left.png" alt="">
							</button>
							<ul class="galery_padign">
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
							</ul>
							<button class="galery_arrw">
								<img src="<?= get_template_directory_uri(); ?>/img/arw_right.png" alt="">
							</button>
						</div>
					</div>
					<div class="row text-center">
						<a href="#" class="galery_link">
							показать все
						</a>
					</div>

				</div>
			</section>
<?php get_footer(); ?>
