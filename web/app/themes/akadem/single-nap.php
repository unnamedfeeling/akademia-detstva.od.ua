<?php get_header(); ?>

<img src="<?php echo get_field('bg_img_nap')['url']; ?>" class="full-w" alt="<?php echo get_field('bg_img_nap')['alt']; ?>">
      <section class="grey">
        <div class="container">
          <div class="row">
            <ul class="bred">
              <?= bcn_display_list(true) ?>
            </ul>
          </div>
        </div>
      </section>
      <section class="news_single">
        <img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec1.png" alt="" class="news_dec news_dec-1">
        <img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec2.png" alt="" class="news_dec news_dec-2">
        <div class="container">
          <div class="news_text">

            <?php foreach( get_field('text_blocks_nap') as $text_blocks_nap ): ?>
            <div class="row va-middle">

              <?php if($text_blocks_nap['position_img']=='left'): ?>
              <div class="col-xs-12 col-sm-6">
                <img src="<?php echo $text_blocks_nap['img']['url']; ?>" alt="<?php echo $text_blocks_nap['img']['alt']; ?>">
              </div>
              <?php endif; ?>

               <div class="col-xs-12 col-sm-6">
				<?php if($text_blocks_nap['html_title_type'] == 'h1' && !empty($text_blocks_nap['title'])):?>
					<h1 class="news_name text-center"><?php echo strip_tags($text_blocks_nap['title']); ?></h1>
				<?php elseif($text_blocks_nap['html_title_type'] == 'h2' && !empty($text_blocks_nap['title'])): ?>
					<h2 class="news_name text-center"><?php echo strip_tags($text_blocks_nap['title']); ?></h2>
				<?php elseif($text_blocks_nap['html_title_type'] == 'h3' && !empty($text_blocks_nap['title'])): ?>
					<h3 class="news_name text-center"><?php echo strip_tags($text_blocks_nap['title']); ?></h3>
				<?php elseif(!empty($text_blocks_nap['title'])): ?>
					<div class="news_name text-center"><?php echo strip_tags($text_blocks_nap['title']); ?></div>
				<?php endif; ?>
                <p><?php echo $text_blocks_nap['text']; ?></p>
              </div>

              <?php if($text_blocks_nap['position_img']=='right'): ?>
              <div class="col-xs-12 col-sm-6">
                <img src="<?php echo $text_blocks_nap['img']['url']; ?>" alt="<?php echo $text_blocks_nap['img']['alt']; ?>">
              </div>
              <?php endif; ?>

            </div>
            <?php endforeach; ?>

          </div>
        </div>
        <div class="space" style="height: 15em"></div>
      </section>
      <section class="bluebg">
        <div class="bluebg_head unload " data-add="<?= get_template_directory_uri(); ?>/mg/cut/btop.png" >   </div>
        <div class="posr">
          <div class="bluebg_cont">
            <section class="team">
              <div class="team_head unload " data-add="<?= get_template_directory_uri(); ?>/img/cut/btop.png" >   </div>
              <div class="posr">
                <div class="team_cont">
                  <p class="h1">
                    <?php the_field('title_advantages_nap'); ?>
                  </p>
                  <div class="container ">
                    <div class="row">

                      <?php foreach( get_field('advantages_nap') as $advantages_nap ): ?>
                      <div class="col-xs-12 col-sm-3">
                        <div class="tadv_el">
                          <div class="tadv_top">
                            <img src="<?= get_template_directory_uri(); ?>/img/squere.jpg" class="sample" alt="">
                            <i class="<?php echo $advantages_nap['icon']; ?>"></i>
                          </div>
                          <div class="adv_text nodec">
                            <p class="bold"><?php echo $advantages_nap['text']; ?></p>

                          </div>
                        </div>
                      </div>
                      <?php endforeach; ?>

                    </div>
                  </div>
                </div>
              </div>
              <div class="team_footer unload " data-add="<?= get_template_directory_uri(); ?>/img/cut/bbott.png" >   </div>
            </section>
          </div>
        </div>
        <div class="bluebg_footer unload " data-add="<?= get_template_directory_uri(); ?>/img/cut/bbott.png" >   </div>
      </section>
      <section class="gnap_team">
        <img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec1.png" alt="" class="gnap_team-dec gnap_team-dec-1">
        <section class="team">
          <div class="posr">
            <div class="container team_cont">
              <div class="posr">
                <p class="h1"><?php the_field('title_our_team_nap'); ?></p>
                <div class="team_slider">

                  <?php foreach( get_field('list_employees_nap') as $list_employees_nap ): ?>
                  <div class="team_slide">
                    <div class="team_el">
                      <div class="team_photo">
                        <div class="team_img unload" data-dadd="<?php echo get_field('photo',$list_employees_nap['human'])['url']; ?>">
                          <img src="<?= get_template_directory_uri(); ?>/img/squere.jpg" class="sample" alt="">
                        </div>
                        <img src="<?= get_template_directory_uri(); ?>/img/dec/team_pdec.png" class="team_photo-decor">
                      </div>
                      <div class="team_text">
                        <p class="team_name"><?php echo get_the_title($list_employees_nap['human']); ?></p>
                        <p class="team_pos"><?php echo get_field('position',$list_employees_nap['human']); ?></p>
                      </div>
                    </div>
                  </div>
                  <?php endforeach; ?>

                </div>
                <div class="slider-dots text-right"></div>
              </div>
              <div class="slider-btn"></div>
            </div>
          </div>
        </section>
      </section>
      <section class="pinkblock">
        <div class="pinkblock_head ">  </div>
        <div class="pinkblock_cont">
          <div class="container">
            <div class="row va-middle full-w">
              <div class="col-xs-12 col-sm-9">
                <p class="pinkblock_h1"><?php the_field('text_feedback_nap'); ?></p>
              </div>
              <div class="col-xs-12 col-sm-3 text-center">
                <button class="btn pinkblock_btn js-popup">
                  <?php the_field('text_button_feedback_nap'); ?>
                </button>
              </div>
            </div>

          </div>
        </div>
        <div class="pinkblock_footer">  </div>
      </section>
      <section class="gnap_galery">
        <section class="galery">
          <div class="container">
            <p class="h1">
              <?php the_field('title_photo_nap'); ?>
            </p>
            <div class="posr">
              <div class="galery_slider">

                <?php foreach( get_field('list_photo_nap') as $list_photo_nap ): ?>
                <div class="galery_slide">
                  <div class="galery_el" onclick="galery(this)" data-large="<?php echo $list_photo_nap['full_img']['url']; ?>">
                    <img src="<?php echo $list_photo_nap['thumb']['url']; ?>" alt="<?php echo $list_photo_nap['thumb']['url']; ?>">
                    <i class="icon-zoom-in"></i>
                  </div>
                </div>
                <?php endforeach; ?>

              </div>
              <div class="slider-dots text-right"></div>
            </div>
            <div class="slider-btn"></div>
          </div>
          <div class="galery-popup">
            <div class="middler ">
              <img src="<?= get_template_directory_uri(); ?>/img/loader.gif" alt="">

            </div>
          </div>
        </section>
      </section>
      <section class="rev">
        <div class="rev_head ">  </div>
        <div class="rev_cont">
          <div class="container">
            <p class="h1"><?php the_field('title_reviews_nap'); ?></p>
          </div>
          <div class="posr">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-xs-10 col-xs-offset-1">
                  <div class="rev_slider">

                    <?php foreach( get_field('list_reviews_nap') as $list_reviews_nap ): ?>
                    <div class="rev_slide">
                      <div class="rev_el">
                        <div class="rev_img">
                          <img src="<?php echo get_field('photo_reviews',$list_reviews_nap['review'])['url']; ?>" alt="<?php echo get_field('photo_reviews',$list_reviews_nap['review'])['alt']; ?>">
                        </div>
                        <div class="rev_info">

                          <div class="rev_el-top">

                            <div class="rev_nm">
                              <div class="rev_name"><?php echo get_the_title($list_reviews_nap['review']); ?></div>
                              <div class="rev_pos"><?php echo get_field('position_reviews',$list_reviews_nap['review']); ?></div>
                            </div>
                            <div class="rev_date"><?php echo get_the_time('j F, Y', $list_reviews_nap['review']); ?></div>
                          </div>
                          <div class="rev_text">
                            <div class="rev_thider">
                              <p><?php echo get_field('text_reviews',$list_reviews_nap['review']); ?></p>
                            </div>
                          </div>
                          <!-- <ul class="rev_bottom">
                            <li>
                              <i class="icon-share-square" ></i>
                              <p>3</p>
                            </li>
                            <li>
                              <i class="icon-heart-empty" ></i>
                              <p>8</p>
                            </li>

                          </ul> -->

                        </div>
                      </div>
                    </div>
                    <?php endforeach; ?>

                  </div>
                </div>
              </div>
            </div>
            <div class="slider-btn"></div>
          </div>
		  <div class="container"><?=do_shortcode( '[kkstarratings]' )?></div>
        </div>
        <div class="rev_footer">  </div>
      </section>
      <section class="bbl">
        <div class="bbl_img">
        </div>
        <div class="bbl_cont">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <p class="h1 bbl_h1"><?php the_field('title_call_to_action_nap'); ?></p>
                <div class="bbl_text">
                  <p><?php the_field('text_call_to_action_nap'); ?></p>
                </div>
                <button class="btn bbl_btn js-popup"><?php the_field('text_button_call_to_action_nap'); ?></button>
              </div>
            </div>


          </div>
        </div>
      </section>

<?php get_footer(); ?>
