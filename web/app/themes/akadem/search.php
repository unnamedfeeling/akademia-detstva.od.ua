<?php get_header(); ?>

      <img src="<?= get_template_directory_uri(); ?>/img/news_top.jpg" class="full-w" alt="">
      <section class="grey">
        <div class="container">
          <div class="row">
            <ul class="bred">
              <?= bcn_display_list(true) ?>
            </ul>
          </div>
        </div>
      </section>
      <section class="news">
        <img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec1.png" alt="" class="news_dec news_dec-1">
        <img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec2.png" alt="" class="news_dec news_dec-2">
        <div class="news_cont">
          <div class="container">
            <p class="h1">
              Поиск
            </p>
            <div class="news_loop">

              <div class="row">
                <div class="col-xs-12 col-sm-9">

                  <?php if( have_posts() ){ while( have_posts() ){ the_post(); ?>
                  <div class="news_el">
                    <div class="news_ls">
                      <div class="news_photo">
                        <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(''); ?>" alt=""></a>
                      </div>
                    </div>
                    <div class="news_rs">
                      <div class="news_inf">
                        <a href="<?php the_permalink(); ?>"><p class="news_name">
                          <?php the_title(); ?>
                        </p></a>
                        <p class="news_time">
                          <i class="icon-clock-1"></i>
                          <?php the_time('g:i, j F, Y' ); ?>
                        </p>
                        <div class="news_text">
                          <p>
                            <?php the_field('short_desc'); ?>
                          </p>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="btn news_btn ">
                          подробнее >
                        </a>
                      </div>
                    </div>
                  </div>
                  <?php } } wp_reset_postdata(); ?>

                </div>
                <div class="col-xs-12 col-sm-3">
                  <div class="search">
                    <?php get_search_form(); ?>
                  </div>
                  <div class="sidebar">
                    <p class="sidebar_h1">Недавние</p>
                    <?php
                    $args = array('post_type' => 'news', 'numberposts' => 6);
                    $myposts = get_posts( $args );
                    foreach( $myposts as $post ){ setup_postdata($post); ?>
                    <div class="sidebar_el">
                      <div class="sidebar_img">
                        <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="">
                      </div>
                      <div class="sidebar_text">
                        <a href="<?php the_permalink(); ?>" class="sidebar_name"><?php the_title(); ?></a>
                        <p class="news_time">
                          <i class="icon-clock-1"></i>
                          <?php the_time('g:i, j F, Y' ); ?>
                        </p>
                      </div>
                    </div>
                    <?php } wp_reset_postdata(); ?>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="news_footer">

        </div>
      </section>

      <section class="map">
        <div class="gmap unload"></div>
      </section>

<?php get_footer(); ?>
