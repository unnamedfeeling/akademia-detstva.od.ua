<?php
get_header();
$tpl=get_template_directory_uri();
?>
<img src="<?= $tpl ?>/img/gal_top.jpg" class="full-w" alt="">
<section class="grey">
	<div class="container">
		<div class="row">
			<ul class="bred">
				<?= bcn_display_list(true) ?>
			</ul>
		</div>
	</div>
</section>
<section class="galery">
	<img src="<?= $tpl ?>/img/dec/team_dec1.png" alt="" class="news_dec news_dec-1">
	<img src="<?= $tpl ?>/img/dec/team_dec1.png" alt="" class="news_dec news_dec-2">
	<img src="<?= $tpl ?>/img/dec/team_dec1.png" alt="" class="galery_dec galery_dec-3">
	<div class="container">
		<p class="h1">Альбом</p>
		<div class="galery_top js-galery_top">
			<?php $i=1; foreach( get_field('cat_gallery') as $cat_gallery ): ?>
			<label for="sw<?php echo $i; ?>" class="<?php if($i==1) echo "active"; ?> galery_btn" >
				<?php echo $cat_gallery['name_cat']; ?>
			</label>
			<?php $i++; endforeach; ?>
		</div>
		<div class="switcher gal">

			<?php $i=1; foreach( get_field('cat_gallery') as $cat_gallery ): ?>
			<div class="switcher_el <?php if($i==1) echo "active"; ?>">
				<input type="radio" id="sw<?php echo $i; ?>" name="Switch1" <?php if($i==1) echo 'checked=""'; ?> >
				<div class="switcher_cont">
					<div class="gal_slider">
						<?php foreach( $cat_gallery['gallery'] as $gallery ): ?>
						<div class="galery_slide">
							<div class="galery_el" onclick="galery(this)" data-large="<?= $gallery['url']; ?>">
								<img src="<?=$tpl?>/img/logo.png" data-lazy="<?= $gallery['sizes']['gallery-img']; ?>" alt="<?= $gallery['alt']; ?>">
								<i class="icon-zoom-in"></i>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
					<div class="gal_slider-btn">
						<div class="posr"></div>
					</div>
				</div>
			</div>
			<?php $i++; endforeach; ?>

		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				<?php
				echo do_shortcode( '[kkstarratings]' );
				// <!-- <button class="galery_arrw">
				// 	<img src="img/arw_left.png" alt="">
				// </button> -->
				// <!-- <ul class="galery_padign">
				// 	<li class="active"><a href="#">1</a></li>
				// 	<li><a href="#">2</a></li>
				// 	<li><a href="#">3</a></li>
				// 	<li><a href="#">4</a></li>
				// 	<li><a href="#">5</a></li>
				// </ul> -->
				// <!-- <button class="galery_arrw">
				// 	<img src="img/arw_right.png" alt="">
				// </button> -->
				?>
			</div>
		</div>


	</div>
	<div class="galery-popup">
		<div class="middler ">
			<img src="<?= $tpl ?>/img/loader.gif" alt="">
		</div>
	</div>
</section>

<?php get_footer(); ?>
