<?php get_header();
$tpl=get_template_directory_uri();
?>
			<section class="fzf">
				<div class="fzf_cont">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-6">
								<div class="fzf_logo">
									<a href="<?= get_home_url() ?>">
										<img src="<?= $tpl ?>/img/logo_akdet.png" alt="">
									</a>
								</div>

								<div class="fzf_wind">
									<p class="fzf_p">ошибка</p>
									<div class="fzf_dec" title="404"><img src="<?= $tpl ?>/img/dec/dec_404.png" alt=""></div>
									<p class="fzf_p">страница не найдена</p>
								</div>
							</div>

						</div>
					</div>

				</div>
			</section>

		</main>
		<!-- CONTENT EOF   -->

	</div>
	<!-- BODY EOF   -->
	<div class="icon-load"></div>
	<?php wp_footer(); ?>
</body>
</html>
