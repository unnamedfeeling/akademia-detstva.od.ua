<?php

get_header();
$main_slider=get_field('slider_main', 164);
$tpl=get_template_directory_uri();
// print_r(get_field('slider_main', 164));
?>
<section class="top">
	<div class="posr">
		<div class="top_slider">
			<?php if (!empty($main_slider)):
							foreach ($main_slider as $img) {
								// print_r($img);
								printf(
									'<div class="top_slide" data-link="%s"><img src="%s" data-lazy="%s" alt="%s"></div>',
									$img['slide-link'],
									$tpl.'/img/pl.jpg',
									$img['img']['url'],
									$img['img']['alt']
								);
							}
						else: ?>
			<div class="top_slide">
				<img src="<?= $tpl; ?>/img/slide.jpg" alt="">
			</div>
			<div class="top_slide">
				<img src="<?= $tpl; ?>/img/slide.jpg" alt="">
			</div>
			<div class="top_slide">
				<img src="<?= $tpl; ?>/img/slide.jpg" alt="">
			</div>
			<?php endif; ?>
		</div>
		<div class="top_dots text-right">
			<div class="container">
				<div class="top_slider-dots"></div>
			</div>
		</div>
		<div class="top_slider-btn"></div>
		<div class="tour3d">
			<div class="dec js-3dTour" data-iframe="https://roundme.com/embed/303908/976551">
                <span>онлайн - обзор</span>
            </div>
			<?php /*<div class="tour-addr">ул. Фонтанская дорога, 49</div>
						<div class="tour-addr active">ул. Новобереговая, 14</div>
						<div class="tour-addr one" data-featherlight='.tour3d-iframe.one' data-featherlight-variant="tour3d-wrap">ул. Семена Яхненка, 14</div>*/ ?>
		</div>
	</div>
</section>
<section class="advantages">
	<div class="advantages_head " data-add="<?= $tpl; ?>/img/cut/ttop.png"> </div>
	<div class="posr">
		<img src="<?= $tpl; ?>/img/dec/dec1.jpg" class="advantages_dec advantages_dec-1"
			onerror=" $(this).onerrorimg(); " alt="">
		<img src="<?= $tpl; ?>/img/dec/dec2.jpg" class="advantages_dec advantages_dec-2"
			onerror=" $(this).onerrorimg(); " alt="">
		<img src="<?= $tpl; ?>/img/dec/dec3.jpg" class="advantages_dec advantages_dec-3"
			onerror=" $(this).onerrorimg(); " alt="">
		<div class="container ">
			<div class="row">

				<?php foreach( get_field('list_main_advantages',164) as $list_main_advantages ): ?>
				<div class="col-xs-12 col-sm-3">
					<div class="advantages_el">
						<div class="advantages_img unload"
							data-dadd="<?php echo $list_main_advantages['img']['url']; ?>">
							<img src="<?php echo $list_main_advantages['img']['url']; ?>"
								alt="<?php echo $list_main_advantages['img']['alt']; ?>"
								onerror=" $(this).onerrorimg(); " class="sample">
						</div>
						<div class="advantages_text">
							<div class="advantages_dev"></div>
							<p><?php echo $list_main_advantages['short_desc']; ?></p>
						</div>
						<a href="<?php echo $list_main_advantages['link']; ?>" class="btn">
							подробнее >
						</a>
					</div>
				</div>
				<?php endforeach; ?>

			</div>
		</div>

		<div class="slider-dots text-center"></div>
		<div class="slider-btn"></div>
	</div>

</section>
<section class="bluebg">
	<img src="<?= $tpl; ?>/img/dec/team_dec1.png" alt="" class="bluebg_dec bluebg_dec-1">
	<img src="<?= $tpl; ?>/img/dec/team_dec2.png" alt="" class="bluebg_dec bluebg_dec-2">
	<div class="bluebg_head unload " data-add="<?= $tpl; ?>/img/cut/btop.png"> </div>
	<div class="posr">
		<div class="bluebg_cont">
			<section class="team">
				<div class="posr">
					<div class="container team_cont">
						<div class="posr">
							<p class="h1"><?php echo get_field('title_our_team_main',164); ?></p>
							<div class="team_slider">

								<?php foreach( get_field('list_our_team_main',164) as $list_our_team_main ): ?>
								<div class="team_slide">
									<div class="team_el">
										<div class="team_photo">
											<div class="team_img unload"
												data-dadd="<?php echo get_field('photo',$list_our_team_main['associate'])['url']; ?>">
												<img src="<?= $tpl; ?>/img/squere.jpg" class="sample" alt="">
											</div>
											<img src="<?= $tpl; ?>/img/dec/team_pdec.png" class="team_photo-decor">
										</div>
										<div class="team_text">
											<p class="team_name">
												<?php echo get_the_title($list_our_team_main['associate']); ?></p>
											<p class="team_pos">
												<?php echo get_field('position',$list_our_team_main['associate']); ?>
											</p>
										</div>
									</div>
								</div>
								<?php endforeach; ?>

							</div>
							<div class="slider-dots text-right"></div>
						</div>
						<div class="slider-btn"></div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="bluebg_footer unload " data-add="<?= $tpl; ?>/img/cut/bbott.png"> </div>
</section>

<section class="sl">
	<div class="sl_cont">
		<img src="<?= $tpl; ?>/img/sl_d1.jpg" alt="" class="sl_dec sl_dec-1">
		<div class="container ">
			<div class="row va-middle turn-phone">
				<div class="col-xs-12 col-sm-5">
					<p class="sl_h1"><?php echo get_field('title_photo_gallery_main',164); ?></p>
					<div class="sl_text">
						<p>
							<?php echo get_field('desc_photo_gallery_main',164); ?>
						</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-2"></div>
				<div class="col-xs-12 col-sm-5">
					<div class="vslider">
						<div class="vslider-slick">

							<?php foreach( get_field('list_photo_gallery_main',164) as $list_photo_gallery_main ): ?>
							<div class="vslider_slide">
								<div class="vslider_el">
									<img src="<?php echo $list_photo_gallery_main['photo']['url']; ?>"
										alt="<?php echo $list_photo_gallery_main['photo']['alt']; ?>">
								</div>
							</div>
							<?php endforeach; ?>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="sl_cont">
		<img src="<?= $tpl; ?>/img/sl_d2.jpg" alt="" class="sl_dec sl_dec-2">
		<div class="container ">
			<div class="row va-middle full-w">
				<div class="col-xs-12 col-sm-6">

					<div class="hor-large">
                        <?php foreach( get_field('list_video_gallery_main',164) as $list_video_gallery_main ):
                        printf(
                            '<div class="hor-large_item"><div class="js-youtube-player" data-id="%s"%s></div></div>',
                            $list_video_gallery_main['id'],
                            !empty($list_video_gallery_main['thumb']['sizes']['medium']) ? ' data-poster="'.$list_video_gallery_main['thumb']['sizes']['medium'].'"' : ''
                        );
                        endforeach; ?>
					</div>

				</div>
				<div class="col-xs-12 col-sm-1"></div>
				<div class="col-xs-12 col-sm-5 col-sm-offset-1 ">
					<p class="sl_h1 sl_h1-pink"><?php echo get_field('title_video_gallery_main',164); ?></p>
					<div class="sl_text">
						<p>
							<?php echo get_field('desc_video_gallery_main',164); ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>


<section class="nap">
	<img src="<?= $tpl; ?>/img/dec/team_dec1.png" alt="" class="nap_decor nap_decor-1">

	<div class="nap_head"> </div>

	<div class="nap_cont">
		<img src="<?= $tpl; ?>/img/dec/nap_dec.png" alt="" class="nap_decor">
		<div class="nap_pos">
			<div class="container ">
				<p class="h1">
					<?php echo get_field('title_nap_main',164); ?>
				</p>


				<div class="nap_slider-cont">
					<div class="nap_slider">

						<?php foreach( get_field('list_nap_main',164) as $list_nap_main ): ?>
						<div class="nap_slide">
							<div class="row va-middle turn-phone">
								<div class="col-xs-12 col-sm-4">
									<p class="sl_h1"><?php echo $list_nap_main['title']; ?></p>
									<div class="sl_text">
										<p>
											<?php echo $list_nap_main['desc']; ?>
										</p>
										<a href="<?php echo get_permalink( $list_nap_main['link'] ); ?>"
											class="btn nap_btn">подробнее ></a>
									</div>
								</div>
								<div class="col-xs-12 col-sm-8">
									<div class="nap_el">
										<div class="nap_img">
											<img src="<?php echo $list_nap_main['img']['url']; ?>"
												alt="<?php echo $list_nap_main['img']['alt']; ?>">
										</div>
										<div class="nap_png">
											<?php if (!empty($list_nap_main['nap-decor']['url'])): ?>
											<img src="<?= $list_nap_main['nap-decor']['url'] ?>" alt="">
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; ?>

					</div>


				</div>
				<div class="nap_slider-dots">
					<div class="nap_dotsslider">
					</div>
				</div>

			</div>
			<div class="container">
				<?=do_shortcode( '[kkstarratings]' )?>
			</div>
		</div>
	</div>
	<div class="nap_footer"> </div>
</section>

<style type="text/css">
.fili{position:relative;z-index:2}
.fili_pos{position:relative;top:-8.42857143em;z-index:3}
.fili_decor{position:absolute;left:0;top:-2.14285714em;bottom:0;height:auto;display:block;z-index:0;max-height:100%}
.fili_decor-1{top:-8em;z-index:2;left:6em}
.fili_cont{background:#fedbe7}
.fili_footer,.fili_head{background-size:100% 100%;background-repeat:no-repeat;position:absolute;width:100%}
.fili_head{height:13.42857143em;bottom:100%;background-image:url(<?=$tpl?>/img/cut/pink_top.png);background-position:bottom;margin-bottom:-.07142857em}
.fili_footer{height:5.92857143em;top:100%;background-image:url(<?=$tpl?>/img/cut/pink_fot.png);background-position:top;margin-top:-.07142857em}
.fili_btn{background:0 0;color:#2561ab;margin-top:1.78571429em}
.fili_btn.active,.fili_btn:hover{background:#2561ab;color:#fedbe7}
.fili_el{position:relative}
.fili_img{max-width:100%;border:.71428571em solid #fff;border-radius:.21428571em;overflow:hidden}
.fili_img img{width:100%}
.fili_slide{padding:1.78571429em 0}
.fili_png{position:absolute;top:0;bottom:0;right:10%;margin-top:-4%;margin-bottom:-4%}
.fili_png img{width:auto;height:100%}
.fili_slider-cont{display:inline-block;width:100%;margin-right:-6.14285714em;padding-right:6.14285714em;vertical-align:middle}
.fili_slider-dots{display:inline-block;width:5.71428571em;vertical-align:middle}
.fili_slider-dots .slick-arrow{position:absolute;left:0;right:0;font-size:1.42857143em;height:2.1em;max-width:100%;opacity:.3;-webkit-transition:opacity .3s;transition:opacity .3s}
.fili_slider-dots .slick-arrow:hover{opacity:1}
.fili_slider-dots .slick-prev{bottom:100%}
.fili_slider-dots .slick-next{top:100%}
.fili_slider .slick-dots{display:none!important}
.fili_dotsslider-slide{text-align:center;font-size:4em;color:#225eaa;font-family:Adigiana2;display:inline-block;width:auto;-webkit-transform:scale(.5);transform:scale(.5);opacity:.3;-webkit-transition:opacity .3s,-webkit-transform .3s;transition:opacity .3s,-webkit-transform .3s;transition:transform .3s,opacity .3s;transition:transform .3s,opacity .3s,-webkit-transform .3s;cursor:pointer}
.fili_dotsslider .slick-current .fili_dotsslider-slide{-webkit-transform:scale(1);transform:scale(1);opacity:1}@media (max-width:767px){.fili_pos{top:0}
.fili_footer,.fili_head{background-size:100%}}
</style>

<section class="fili">
    <img src="<?= $tpl; ?>/img/dec/team_dec1.png" alt="" class="fili_decor fili_decor-1">

    <div class="fili_head"> </div>

    <div class="fili_cont">
        <img src="<?= $tpl; ?>/img/dec/nap_dec.png" alt="" class="fili_decor">
        <div class="fili_pos">
            <div class="container ">
                <p class="h1">
                    <?php echo get_field('title_filias_main',164); ?>
                </p>


                <div class="fili_slider-cont">
                    <div class="fili_slider">

                        <?php foreach( get_field('list_filias_main',164) as $list_filias_main ): ?>
                        <div class="fili_slide">
                            <div class="row va-middle turn-phone">
                                <div class="col-xs-12 col-sm-4">
                                    <p class="sl_h1"><?php echo $list_filias_main['title']; ?></p>
                                    <div class="sl_text">
                                        <p>
                                            <?php echo $list_filias_main['desc']; ?>
                                        </p>
                                        <a href="<?php echo get_permalink( $list_filias_main['link'] ); ?>"
                                            class="btn fili_btn">подробнее ></a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="fili_el">
                                        <div class="fili_img">
                                            <img src="<?php echo $list_filias_main['img']['url']; ?>"
                                                alt="<?php echo $list_filias_main['img']['alt']; ?>">
                                        </div>
                                        <div class="fili_png">
                                            <?php if (!empty($list_filias_main['filias-decor']['url'])): ?>
                                            <img src="<?= $list_filias_main['filias-decor']['url'] ?>" alt="">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>

                    </div>


                </div>
                <div class="fili_slider-dots">
                    <div class="fili_dotsslider">
                    </div>
                </div>

            </div>
            <div class="container">
                <?=do_shortcode( '[kkstarratings]' )?>
            </div>
        </div>
    </div>
    <div class="fili_footer"> </div>
</section>

<script type="text/javascript">
var i,n=$(".fili_slider");
n.on("afterChange",function(i,t,e){console.log(n.find("[data-slick-index="+e+"]").innerHeight()+"px"),n.find(".slick-list").height(n.find("[data-slick-index="+e+"]").innerHeight()+"px")}),n.slick({arrows:!1,asNavFor:$(".fili_dotsslider"),cssEase:"ease",customPaging:function(i,t){return t=++t+1<10?"0"+t.toString():t.toString(),$(".fili_dotsslider").html($(".fili_dotsslider").html()+'<div class="fili_dotsslider-slide">'+t+"</div>"),""},dots:!0,vertical:!0,verticalSwiping:!0}),$(".fili_dotsslider").slick({arrows:!0,asNavFor:$(".fili_slider"),prevArrow:'<button class="slick-prev slick-arrow" aria-label="Previous" type="button"> <img src="'+ajax_func.tpld+'/img/arw_up.png" alt="" /> </button>',nextArrow:'<button class="slick-next slick-arrow" aria-label="Next" type="button"> <img src="'+ajax_func.tpld+'/img/arw_dwn.png" alt="" /> </button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!0,centerPadding:"0px",cssEase:"ease",slidesToShow:3,slidesToScroll:1,swipeToSlide:!0,touchMove:!0,vertical:!0,verticalSwiping:!0,waitForAnimate:!0,focusOnSelect:!0});
</script>

<section class="map">
	<div class="gmap unload"></div>
</section>
<section class="iblk">
	<div class="iblk_head"> </div>
	<div class="iblk_bg">
		<div class="container">
			<p class="h1 iblk_h1">&nbsp;</p>
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1">
					<?php echo do_shortcode('[contact-form-7 id="244" title="Contact form price"]'); ?>
					<p class="form_p"><?php echo get_field('text_after_form_main',164); ?></p>
				</div>

			</div>
		</div>
	</div>
</section>

<?php
	// <!--
	// 		<section class="test">
	// 	<div class="head test"> <div class="test">head</div> </div>
	// 	<div class="container">
	// 		<div class="row">
	// 			<div class="content test"> <div class="test"> cont</div> </div>
	// 		</div>
	// 	</div>
	// 	<div class="footer test"> <div class="test">footer</div> </div>
	// </section>
	//
	// 		<section>
	// 			<div class="test_slider">
	// 				<div class="test_slide">
	// 					<img src="<?= $tpl; /img/slide.jpg" alt="">
	// 				</div>
	// 				<div class="test_slide">
	// 					<img src="<?= $tpl; /img/slide.jpg" alt="">
	// 				</div>
	// 				<div class="test_slide">
	// 					<img src="<?= $tpl; /img/slide.jpg" alt="">
	// 				</div>
	// 				<div class="test_slide">
	// 					<img src="<?= $tpl; /img/slide.jpg" alt="">
	// 				</div>
	// 			</div>
	// 		</section>
	// 		<section class="test">
	// 			<div class="test_head "> head </div>
	// 			<div class="container">
	// 				<div class="row">
	//
	// 				</div>
	// 			</div>
	// 			<div class="test_footer"> footer </div>
	// 		</section>
	// 	-->


get_footer(); ?>