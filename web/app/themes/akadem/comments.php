<?php
	/**
	 * The template for displaying comments
	 *
	 * If the current post is protected by a password and
	 * the visitor has not yet entered the password we will
	 * return early without loading the comments.
	 */
	if ( post_password_required() )
	{
		return;
	}
?>

<div id="comments" class="comment-box-container">
	
	<ul class="commentlist">
	<?php
		if ( have_comments() ) : ?>
			<?php
			wp_list_comments( array(
				'style'       => 'li',
				'short_ping'  => true,
				'avatar_size' => 90,
				'callback' => 'my_comments_callback'
			) );
			?>
		
		<?php endif; // have_comments() ?>
	</ul>
	
	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
			?>
			<div class="no-comments"><?php esc_html_e( 'Comments are closed.', 'colosseum' ); ?></div>
		<?php endif;
		paginate_comments_links();
	?>
</div>
