<?php $tpl=get_template_directory_uri(); global $dont_show; ?>
</main>
<!-- CONTENT EOF   -->
<section class="popup ">

	<div class="popup_wind">
		<img src="<?= $tpl; ?>/img/popo_dec1.jpg" alt="" class="popup_dec popup_dec-1">
		<img src="<?= $tpl; ?>/img/popo_dec2.jpg" alt="" class="popup_dec popup_dec-2">
		<div class="popup_wind-cont">
			<p class="popup_h1">оставьте заявку</p>
			<?= do_shortcode('[contact-form-7 id="245" title="Contact Form popup"]'); ?>
		</div>
	</div>
</section>
<style>
.popup-oferta{position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:100;background:rgba(34,94,170,.8);text-align:center;display:inline-block;vertical-align:middle;height:100%;width:100%;opacity:0;visibility:hidden;-webkit-transition:opacity .3s,visibility .3s;transition:opacity .3s,visibility .3s}
.popup-oferta:before{width:0;content:'';vertical-align:middle;height:100%;display:inline-block}
.popup-oferta .middler{display:inline-block;width:100%;vertical-align:middle;margin-left:-.35714286em}
.popup-oferta.active{opacity:1;visibility:visible}
.popup_doc_wrap{flex-wrap:wrap;align-items:center;display:flex;justify-content:space-around;flex:0 1 100%}
.popup_doc{box-sizing:border-box;margin:0;padding:0;border:0;font:inherit;vertical-align:baseline}
.popup_doc{position: relative;display: block;margin: 1rem auto .3rem;}
</style>
<section class="popup-oferta ">

	<div class="popup_wind">
		<img src="<?= $tpl; ?>/img/popo_dec1.jpg" alt="" class="popup_dec popup_dec-1">
		<img src="<?= $tpl; ?>/img/popo_dec2.jpg" alt="" class="popup_dec popup_dec-2">
		<div class="popup_wind-cont">
			<p class="popup_h1">Оферта</p>
			<div class="popup_doc_wrap">
				<div class="popup_doc">
					<a href="<?= $tpl; ?>/doc/pub_dogovor.docx">
						<img height="80" src="<?= $tpl; ?>/img/doc_file_icon.png" class="popup_doc"/>
						ПУБЛИЧНЫЙ ДОГОВОР
					</a>
				</div>

				<div class="popup_doc">
					<a href="<?= $tpl; ?>/doc/zayzvlenie.docx">
						<img height="80" src="<?= $tpl; ?>/img/doc_file_icon.png" class="popup_doc"/>
						Заявление для поступления
					</a>
				</div>
				
				<div class="popup_doc">
					<a href="<?= $tpl; ?>/doc/pravila.docx">
						<img height="80" src="<?= $tpl; ?>/img/doc_file_icon.png" class="popup_doc"/>
						Правила посещения детского сада
					</a>
				</div>

				<div class="popup_doc">
					<a href="<?= $tpl; ?>/doc/dodatok_na_font.docx">
						<img height="80" src="<?= $tpl; ?>/img/doc_file_icon.png" class="popup_doc"/>
						Додаток на Фонтанскую дор.
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- BEGIN HEADER -->
<header class="header">
	<div class="header_lcont">
		<div class="container ">
			<div class="row">
				<div class="col-xs-12 col-sm-2 text-center posr">
					<div class="header_logo header_logo-mod">
						<?php //<!-- <img src="<?= $tpl; /img/logo.svg" onerror=" $(this).onerrorimg(); " data-src="<?= $tpl; /img/logo.png" alt="#"> --> ?>
						<a href="/"><img src="<?= get_field('logo_header',164)['url']; ?>"
								alt="<?= get_field('logo_header',164)['alt']; ?>"></a>
						<?php
				  // <!-- <p>
	              //   Академия <br>
	              //   детства
	              // </p> -->
				  ?>
					</div>
					<div class="header_bcont-btn">
						<button class="header_btn">
							<span></span>
							<span></span>
							<span></span>
						</button>
					</div>
					<img src="<?= $tpl; ?>/img/logoconer.png" class="header_coner" alt="#" class="decor">
				</div>
				<div class="col-xs-12 col-sm-10">
					<div class="header_top-decor"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="header_tcont">
		<div class="container ">
			<div class="row">
				<div class="col-xs-12 col-sm-2"></div>
				<div class="col-xs-12 col-sm-10">
					<div class="header_top">
						<ul class="header_top-list">
                            <?php  if(($phones = get_field('header_phones', 164)) && empty($phones)) { ?>
							<li class="header_top-item">
								<a class="header_tlink phone"
									href="tel:<?= preg_replace("/[^A-Za-z0-9+]/", '', get_field('phone1_header',164)) ?>">
									<i class="icon-phone"></i>
									<?= get_field('phone1_header',164) ?>
								</a>
							</li>
							<li class="header_top-item">
								<a class="header_tlink phone"
									href="tel:<?= preg_replace("/[^A-Za-z0-9+]/", '', get_field('phone2_header',164)) ?>">
									<i class="icon-phone"></i>
									<?= get_field('phone2_header',164) ?>
								</a>
							</li>
                            <?php } else {
                                foreach ($phones as $phone) {
                                    printf(
                                        '<li class="header_top-item">
                                            <a class="header_tlink phone" href="tel:%s"><i class="icon-phone"></i>%s</a>
                                        </li>',
                                        preg_replace("/[^A-Za-z0-9+]/", '', $phone['phone']),
                                        $phone['phone']
                                    );
                                }
                            } ?>
							<li class="header_top-item">
								<a class="header_tlink mail" href="mailto:<?= get_field('email_header',164); ?>">
									<i class="icon-mail-alt"></i>
									<?= get_field('email_header',164); ?>
								</a>
							</li>
						</ul>
						<ul class="scl">
							<li class="scl_item"> <a href="<?= get_field('link_instagram',164); ?>" class="scl_link"> <i
										class=" icon-instagram"></i> </a> </li>
							<li class="scl_item"> <a href="<?= get_field('link_facebook',164); ?>" class="scl_link"> <i
										class=" icon-facebook"></i> </a> </li>
							<li class="scl_item"> <a href="<?= get_field('link_youtube',164); ?>" class="scl_link"> <i
										class=" icon-youtube-play"></i> </a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="header_bcont">
		<div class="container ">
			<div class="row">
				<div class="col-xs-12 col-sm-2"></div>
				<div class="col-xs-12 col-sm-10 text-right">
					<?php render_top_menu(2, 'Main_Nav_Menu_Walker'); ?>
				</div>
			</div>
		</div>
	</div>

</header>
<!-- HEADER EOF   -->
<!-- BEGIN FOOTER -->
<footer id="footer" class="footer">
	<div class="footer_head <?php echo get_field('show_map')? 'map_bg_fix': ''?>"> </div>
	<div class="footer_cont">
		<div class="container">
			<?php if( (get_field('toggle_seo_text_footer') || is_null(get_field('toggle_seo_text_footer'))) && (!isset($dont_show) || !$dont_show)): ?>
			<div class="row">
				<div class="col-xs-12 text-center">
					<div id="footerhidder" class="footer_hider">
						<div class="footer_text">
							<?php if(!is_null(get_field('nap_seo_text_footer')) && !empty(get_field('nap_seo_text_footer'))): ?>
							<?= get_field('nap_seo_text_footer'); ?>
							<?php else: ?>
							<?= get_field('seo_text_footer',164); ?>
							<?php endif; ?>
						</div>
					</div>
					<button class="footer_btn"><?= get_field('text_button_seo_text',164); ?></button>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
					<p class="footer_h1"><?= get_field('title_footer',164); ?></p>
					<p class="footer_name"><?= get_field('short_title_footer',164); ?></p>
				</div>
			</div>
			<div class="row va-middle">
				<div class="col-xs-12 col-sm-3">
					<div class="footer_el">
						<i class="icon-location"> </i>
						<o class="footer_name">Адреса:</o>
						<div class="footer_text">
							<?php
  					$foot_addr=get_field('footer_addr', 164);
  					if (!empty($foot_addr)) {
  						foreach ($foot_addr as $key => $val) {
  							printf(
  								'<p>%s</p>',
  								$val['addr']
  							);
  						}
  					}
  					?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="footer_el">
						<i class="icon-address-book"> </i>
						<o class="footer_name">Контакты:</o>
						<div class="footer_text">
							<?php
					$foot_phones=get_field('footer_phones', 164);
					if (!empty($foot_phones)) {
						$ind=1;
						foreach ($foot_phones as $key => $val) {
							printf(
								'<a href="tel:%s" class="binct-phone-number-%s">%s</a>',
								preg_replace("/[^A-Za-z0-9+]/", '', $val['phone']),
								$ind,
								$val['phone']
							);
							$ind++;
						}
					}
					?>
							<a
								href="mailto:<?= get_field('email_header',164); ?>"><?= get_field('email_header',164); ?></a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="footer_el">
						<i class="icon-clock"> </i>
						<o class="footer_name">Время работы:</o>
						<div class="footer_text">
							<p><?= get_field('working_hours_footer',164); ?></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="footer_el">
						<o class="footer_name" style="margin-bottom: 0.5rem;">Наши партнёры:</o>
						<img src="<?= $tpl; ?>/img/junior-lg.png" alt="" />
					</div>
				</div>

			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
					<p class="footer_name">Территория застрахована</p>
					<img src="<?= $tpl; ?>/img/ekaterina-cert.jpg" alt="" />
				</div>
			</div>
			<div class="row" style="margin-top: 50px;">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
					<p class="js-popup-oferta footer_name" style="cursor: pointer;">Оферта</p>
				</div>
			</div>
		</div>
	</div>
	<div class="footer_bottom">
		<div class="copy text-center">
			<p>
				<i class="icon-copyright"></i> designed and developed by <a href="//sale-partners.com.ua">Sale Partners</a>
			</p>
		</div>
	</div>
	<img src="<?= $tpl; ?>/img/cut/footer_l.png" alt="" class="footer_left">
	<img src="<?= $tpl; ?>/img/cut/footer_r.png" alt="" class="footer_right">
</footer>
<!-- FOOTER EOF   -->
</div>
<!-- BODY EOF   -->
<?php wp_footer(); ?>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function () {
		var elems = document.getElementsByClassName("phone");
		var elemsTotal = elems.length;
		for (var i = 0; i < elemsTotal; ++i) {
			elems[i].classList.add('binct-phone-number-' + (i + 1))
		}
		/*setTimeout(function(){
		  (function(a,b,c){var d="so3dek53xux745h3ruiy";b=a.createElement(c);b.type="text/javascript";b.async=!0;b.src="//widgets.binotel.com/getcall/widgets/"+d+".js";a=a.getElementsByTagName(c)[0];a.parentNode.insertBefore(b,a)})(document,window,"script");
		  (function(a,b,c){var d="5gaorxtp63rbrneivul6";b=a.createElement(c);b.type="text/javascript";b.async=!0;b.src="//widgets.binotel.com/calltracking/widgets/"+d+".js";a=a.getElementsByTagName(c)[0];a.parentNode.insertBefore(b,a)})(document,window,"script");
		}, 200)*/

	})
</script>

<script>
    window.addEventListener('load', function(){
	    (function (w, d, s, l, i) {
		    w[l] = w[l] || [];
		    w[l].push({
			    'gtm.start': new Date().getTime(),
			    event: 'gtm.js'
		    });
		    var f = d.getElementsByTagName(s)[0],
			    j = d.createElement(s),
			    dl = l != 'dataLayer' ? '&l=' + l : '';
		    j.async = true;
		    j.src =
			    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
		    f.parentNode.insertBefore(j, f);
	    })(window, document, 'script', 'dataLayer', 'GTM-K5CR3JH');
    })
</script>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K5CR3JH" height="0" width="0"
		style="display:none;visibility:hidden"></iframe></noscript>
</body>

</html>