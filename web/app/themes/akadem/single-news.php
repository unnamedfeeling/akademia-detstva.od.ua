<?php get_header();
$img = get_field('top_image', 'option');
if ($img): ?>
	<img src="<?= $img['url'] ?>" class="full-w" alt="">
<?php else: ?>
	<img src="<?= get_template_directory_uri(); ?>/img/news_top.jpg" class="full-w" alt="">
<?php endif; ?>
      <section class="grey">
        <div class="container">
          <div class="row">
            <ul class="bred">
              <?= bcn_display_list(true) ?>
            </ul>
          </div>
        </div>
      </section>
      <section class="news_single">

        <img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec1.png" alt="" class="news_dec news_dec-1">
        <img src="<?= get_template_directory_uri(); ?>/img/dec/team_dec2.png" alt="" class="news_dec news_dec-2">

        <div class="container">
          <p class="h1">
            Новости
          </p>
          <div class="row">
            <div class="col-xs-12 col-sm-9">
              <div class="text-center">
                <h1 class="news_name"><?php the_title(); ?></h1>
                <p class="news_time">
                  <i class="icon-clock-1"></i>
                  <?php the_time('g:i, j F, Y' ); ?>
                </p>

              </div>
              <?php if( have_posts() ){ while( have_posts() ){ the_post(); ?>
              <div class="news_text">
                <div class="row">
                  <div class="col-xs-12">
                    <img src="<?php echo get_field('big_img')['url']; ?>" alt="<?php echo get_field('big_img')['alt']; ?>">
                    <?php the_content(); ?>
                  </div>
                </div>
              </div>
              <?php } } wp_reset_postdata(); ?>
            </div>
            <div class="col-xs-12 col-sm-3">
              <div class="sidebar">
                <p class="sidebar_h1">Недавние</p>
                <?php
                $args = array('post_type' => 'news', 'numberposts' => 6);
                $myposts = get_posts( $args );
                foreach( $myposts as $post ){ setup_postdata($post); ?>
                <div class="sidebar_el">
                  <div class="sidebar_img">
                    <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="">
                  </div>
                  <div class="sidebar_text">
                    <a href="<?php the_permalink(); ?>" class="sidebar_name"><?php the_title(); ?></a>
                    <p class="news_time">
                      <i class="icon-clock-1"></i>
                      <?php the_time('g:i, j F, Y' ); ?>
                    </p>
                  </div>
                </div>
                <?php } wp_reset_postdata(); ?>
              </div>
            </div>
          </div>
        </div>
      </section>
	  <?php if(get_field('show_block_feedback_template')): ?>
	  <section class="blueline">
	  	<div class="blueline_head ">  </div>
	  	<div class="blueline_cont">
	  		<div class="container">
	  			<div class="row va-middle full-w">
	  				<div class="col-xs-12 col-sm-9">
	  					<p class="pinkblock_h1"><?php the_field('text_feedback_template'); ?></p>
	  				</div>
	  				<div class="col-xs-12 col-sm-3 text-center">
	  					<button class="btn pinkblock_btn js-popup">
	  						<?php the_field('text_button_feedback_template'); ?>
	  					</button>
	  				</div>
	  			</div>

	  		</div>
	  	</div>
	  	<div class="blueline_footer">  </div>
	  </section>
	  <?php endif; ?>
	  <?php if (get_field('show_block_photo_template')): ?>
		  <div class="npb">
			  <section class="galery">
				  <div class="container">
					  <p class="h1">
						  <?php the_field('title_photo_template'); ?>
					  </p>
					  <div class="posr">
						  <div class="galery_slider">

							  <?php foreach( get_field('list_photo_template') as $list_photo_template ): ?>
							  <div class="galery_slide">
								<div class="galery_el" onclick="galery(this)" data-large="<?php echo $list_photo_template['full_img']['url']; ?>">
								  <img src="<?php echo $list_photo_template['thumb']['url']; ?>" alt="<?php echo $list_photo_template['thumb']['url']; ?>">
								  <i class="icon-zoom-in"></i>
								</div>
							  </div>
							  <?php endforeach; ?>

						  </div>
						  <div class="slider-dots text-right"></div>
					  </div>
					  <div class="slider-btn"></div>
				  </div>
				  <div class="galery-popup">
					  <div class="middler ">
						  <img src="<?= get_template_directory_uri(); ?>/img/loader.gif" alt="">

					  </div>
				  </div>
			  </section>
		  </div>
	  <?php endif; ?>
	  <?php if (get_field('block_description_true')): ?>
		  <section class="wbl">

			  <div class="container">
				  <div class="row">
					  <div class="row">
						  <div class="col-xs-12 text-center">
							  <h1><?php the_field('title_block_description'); ?></h1>
							  <div class="wbl_text">
								  <?php the_field('desc_before_block_description'); ?>
								  <div id="wblhidder" class="wbl_hidder">
									  <?php the_field('desc_block_description'); ?>
								  </div>
							  </div>
						  </div>
						  <div class="text-center">
							  <button class="wbl_btn"><?php the_field('text_button_description'); ?></button>
						  </div>
					  </div>
				  </div>
			  </div>

		  <div class="wbl_footer">   </div>
		  </section>
	  <?php endif; ?>
	  <?php if(get_field('block_to_action_true')): ?>
	  <section class="bbl">
	  	<div class="bbl_img bbl_img-2">
	  	</div>
	  	<div class="bbl_cont">
	  		<div class="container">
	  			<div class="row">
	  				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
	  					<p class="h1 bbl_h1"><?php the_field('title_block_to_action'); ?></p>
	  					<div class="bbl_text">
	  						<p><?php the_field('desc_block_to_action'); ?></p>
	  					</div>
	  					<?php if(get_field("variant_button")=="popup_form"): ?><button class="btn bbl_btn js-popup"><?php the_field('text_button_to_action'); ?></button>
	  					<?php elseif(get_field("variant_button")=="link"): ?><a href="<?php the_field('link_button_to_action'); ?>" class="btn bbl_btn"><?php the_field('text_button_to_action'); ?></a>
	  					<?php endif; ?>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
	  </section>
	  <?php endif; ?>
      <section class="map">
        <div class="gmap unload"></div>
      </section>

<?php get_footer(); ?>
