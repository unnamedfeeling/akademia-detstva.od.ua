<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0">
  <title><?php wp_title('', true); ?></title>
  <link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/img/favicon.png" type="image/png">
  <!--[if lt IE 9]><script src="js/html5.js"></script><![endif]-->
  <?php wp_head(); ?>
</head>
<body <?php body_class( 'loaded' )?>>
	<div class="icon-load"></div>
  <!-- BEGIN BODY -->
  <!-- BEGIN CONTENT -->
  <div class="main-wrapper ">
    <main class="content js-padboth">
