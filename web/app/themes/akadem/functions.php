<?php

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
function theme_name_scripts() {

	wp_enqueue_style( 'style-main', get_stylesheet_uri() );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.min.css' );
	wp_enqueue_style( 'gfonts', 'https://fonts.googleapis.com/css?family=Montserrat&display=swap' );

	wp_deregister_script( 'jquery' );
	wp_deregister_script( 'jquery-migrate' );

	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/custom.min.js', array('maps'), '', true );
	wp_enqueue_script( 'maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyC1jbrR6982_Z2Ua5ajEMEznvjpBCIvZaU', array(), '', true );
	// wp_enqueue_script( 'maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyD8dqhhhfsmCVAS9Uh_nrFUtKAkE6NsMqs', array(), '', true );
	$data=[
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'tpld' => get_template_directory_uri(),
	];
	global $post;
	$pid = '';
	if(isset($post)) $pid = $post->ID;
	if(is_home() || is_front_page()) $pid = 164;
	if ($pid && !empty(get_field('map_elements', $pid))) {
		$data['mapCoords']=json_encode(get_field('map_elements', $pid));
	}
	wp_localize_script( 'maps', 'ajax_func', $data);
}

add_action( 'after_setup_theme', 'theme_register_nav_menu' );
function theme_register_nav_menu() {
  register_nav_menu( 'primary', 'Primary Menu' );
}

add_theme_support( 'post-thumbnails' );
add_image_size('nap-decor', 181, 439, false);
add_image_size('filias-decor', 181, 439, false);
add_image_size('gallery-img', 400, 267, true);

function remove_menus(){
  //remove_menu_page( 'edit.php?post_type=page' );
  remove_menu_page( 'index.php' );
  remove_menu_page( 'edit.php' );
  remove_menu_page( 'edit-comments.php' );
  //remove_menu_page( 'tools.php' );
  //remove_menu_page( 'upload.php' );
  //remove_menu_page( 'themes.php' );
  //remove_menu_page( 'plugins.php' );
  //remove_menu_page( 'users.php' );
  //remove_menu_page( 'options-general.php' );
  remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
// add_action( 'admin_menu', 'remove_menus' );

function create_post_news() {
	register_post_type( 'news',
		array(
		'labels' => array(
		'name' => __( 'Новости' ),
		'singular_name' => __( 'Новости' ),
		'add_new' => __('Добавить новость'),
		'add_new_item' => __('Добавить новость'),
		'edit_item' => __('Редактировать новость'),
		'new_item' => __('Новая новость'),
		'all_items' => __('Все новости'),
		'parent_item'       => 'Родитель',
		'parent_item_colon' => 'Родитель',
		'view_item' => __('Просмотр новости'),
		'search_items' => __('Поиск новости'),
		'not_found' => __('Не найдено'),
		'not_found_in_trash' => __('Не найдено'),
		'menu_name' => 'Новости'
		),
		'public'              => true,
		    'publicly_queryable'  => true,
		'menu_position' => 5,
		'rewrite' => array('slug' => 'news'),
		'has_archive' => true,
		'hierarchical' => true,
		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes')
		)
	);
}
add_action( 'init', 'create_post_news' );

function create_post_notice() {
	register_post_type( 'notice',
		array(
		'labels' => array(
		'name' => __( 'Заметки для родителей' ),
		'singular_name' => __( 'Заметка' ),
		'add_new' => __('Добавить заметку'),
		'add_new_item' => __('Добавить заметку'),
		'edit_item' => __('Редактировать заметку'),
		'new_item' => __('Новая заметка'),
		'all_items' => __('Все заметки'),
		'parent_item'       => 'Родитель',
		'parent_item_colon' => 'Родитель',
		'view_item' => __('Просмотр заметки'),
		'search_items' => __('Поиск заметки'),
		'not_found' => __('Не найдено'),
		'not_found_in_trash' => __('Не найдено'),
		'menu_name' => 'Заметки для родителей'
		),
		'public'              => true,
		'publicly_queryable'  => true,
		'menu_position' => 5,
		'rewrite' => array('slug' => 'notice'),
		'has_archive' => true,
		'hierarchical' => true,
		'menu_icon' => 'dashicons-welcome-write-blog',
		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes', 'comments')
		)
	);
}
add_action( 'init', 'create_post_notice' );

function create_post_workers() {
	register_post_type( 'workers',
		array(
		'labels' => array(
		'name' => __( 'Сотрудники' ),
		'singular_name' => __( 'Сотрудники' ),
		'add_new' => __('Добавить сотрудника'),
		'add_new_item' => __('Добавить сотрудника'),
		'edit_item' => __('Редактировать сотрудника'),
		'new_item' => __('Новая сотрудника'),
		'all_items' => __('Все Сотрудники'),
		'parent_item'       => 'Родитель',
		'parent_item_colon' => 'Родитель',
		'view_item' => __('Просмотр Сотрудники'),
		'search_items' => __('Поиск Сотрудники'),
		'not_found' => __('Не найдено'),
		'not_found_in_trash' => __('Не найдено'),
		'menu_name' => 'Сотрудники'
		),
		'public'              => true,
		    'publicly_queryable'  => true,
		'menu_position' => 5,
		'rewrite' => array('slug' => 'workers'),
		'has_archive' => true,
		'hierarchical' => true,
		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes')
		)
	);
}
add_action( 'init', 'create_post_workers' );

function create_post_reviews() {
	register_post_type( 'reviews',
		array(
		'labels' => array(
		'name' => __( 'Отзывы' ),
		'singular_name' => __( 'Отзывы' ),
		'add_new' => __('Добавить отзыв'),
		'add_new_item' => __('Добавить отзыв'),
		'edit_item' => __('Редактировать отзыв'),
		'new_item' => __('Новая отзыв'),
		'all_items' => __('Все Отзывы'),
		'parent_item'       => 'Родитель',
		'parent_item_colon' => 'Родитель',
		'view_item' => __('Просмотр Отзывы'),
		'search_items' => __('Поиск Отзывы'),
		'not_found' => __('Не найдено'),
		'not_found_in_trash' => __('Не найдено'),
		'menu_name' => 'Отзывы'
		),
		'public'              => true,
		    'publicly_queryable'  => true,
		'menu_position' => 5,
		'rewrite' => array('slug' => 'reviews'),
		'has_archive' => true,
		'hierarchical' => true,
		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes')
		)
	);
}
add_action( 'init', 'create_post_reviews' );

function create_post_nap() {
	register_post_type( 'nap',
		array(
		'labels' => array(
		'name' => __( 'Направления' ),
		'singular_name' => __( 'Направления' ),
		'add_new' => __('Добавить направление'),
		'add_new_item' => __('Добавить направление'),
		'edit_item' => __('Редактировать направление'),
		'new_item' => __('Новое направление'),
		'all_items' => __('Все Направления'),
		'parent_item'       => 'Родитель',
		'parent_item_colon' => 'Родитель',
		'view_item' => __('Просмотр Направления'),
		'search_items' => __('Поиск Направления'),
		'not_found' => __('Не найдено'),
		'not_found_in_trash' => __('Не найдено'),
		'menu_name' => 'Направления'
		),
		'public'              => true,
		    'publicly_queryable'  => true,
		'menu_position' => 5,
		'rewrite' => array('slug' => 'nap'),
		'capability_type'    => 'page',
		'has_archive' => true,
		'hierarchical' => true,
		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes')
		)
	);
}
add_action( 'init', 'create_post_nap' );

// function create_post_filias() {
// 	register_post_type( 'filias',
// 		array(
// 		'labels' => array(
// 		'name' => __( 'Наши филиалы' ),
// 		'singular_name' => __( 'Наши филиалы' ),
// 		'add_new' => __('Добавить филиал'),
// 		'add_new_item' => __('Добавить филиал'),
// 		'edit_item' => __('Редактировать филиал'),
// 		'new_item' => __('Новый филиал'),
// 		'all_items' => __('Все филиал'),
// 		'parent_item'       => 'Родитель',
// 		'parent_item_colon' => 'Родитель',
// 		'view_item' => __('Просмотр филиала'),
// 		'search_items' => __('Поиск филиала'),
// 		'not_found' => __('Не найдено'),
// 		'not_found_in_trash' => __('Не найдено'),
// 		'menu_name' => 'Филиалы'
// 		),
// 		'public'              => true,
// 		    'publicly_queryable'  => true,
// 		'menu_position' => 6,
// 		'rewrite' => array('slug' => 'filias'),
// 		'capability_type'    => 'page',
// 		'has_archive' => true,
// 		'hierarchical' => true,
// 		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes')
// 		)
// 	);
// }
// add_action( 'init', 'create_post_filias' );

// function create_post_requests() {
// register_post_type( 'requests',
// array(
// 'labels' => array(
// 'name' => __( 'Заявки' ),
// 'singular_name' => __( 'Заявки' ),
// 'add_new' => __('Добавить заявку'),
// 'add_new_item' => __('Добавить заявку'),
// 'edit_item' => __('Редактировать заявку'),
// 'new_item' => __('Новая заявку'),
// 'all_items' => __('Все Заявки'),
// 'parent_item'       => 'Родитель',
// 'parent_item_colon' => 'Родитель',
// 'view_item' => __('Просмотр Заявки'),
// 'search_items' => __('Поиск Заявки'),
// 'not_found' => __('Не найдено'),
// 'not_found_in_trash' => __('Не найдено'),
// 'menu_name' => 'Заявки'
// ),
// 'public'              => true,
//     'publicly_queryable'  => true,
// 'menu_position' => 5,
// 'rewrite' => array('slug' => 'requests'),
// 'has_archive' => true,
// 'hierarchical' => true,
// 'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes')
// )
// );
// }
// add_action( 'init', 'create_post_requests' );

function render_top_menu($menu, $walker=null) {
  // $menu_items = wp_get_nav_menu_items( $menu );
  //
  // foreach ( $menu_items as $menu_item ) {
  //     echo '<li class="header_bcont-item active"><a href="'. get_home_url() . $menu_item->url . '" class="header_blink">' . $menu_item->title . '</a></li>';
  // }

  $args=[
    'menu'			=> $menu,
		'container'	   => null,
		// 'container_class' => 'menu-{menu slug}-container',
		'container_id'	=> '',
		'menu_class'	  => 'menu',
		'menu_id'		 => '',
		'echo'			=> true,
		'fallback_cb'	 => 'wp_page_menu',
		'before'		  => '',
		'after'		   => '',
		'link_before'	 => '',
		'link_after'	  => '',
		'items_wrap'	  => '<ul class="header_bcont-list">%3$s</ul>',
		'depth'		   => 0,
		'walker'		  => new Main_Nav_Menu_Walker()
  ];

  $args['walker']=(!empty($walker))? new $walker : null;

  wp_nav_menu($args);
}

// Menu walker
class Main_Nav_Menu_Walker extends Walker_Nav_Menu{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth     Depth of menu item. May be used for padding.
	 * @param  array $args    Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
    $class=($depth>=1) ? 'mmenu_item' : 'header_bcont-item';
		$class.=($depth==0&&$item->current&&!in_array('headmidmenu_item-logo', $item->classes)) ? ' active':'';
		// $class.=($depth>=1 ? 'header__bottom-sub-menu-link ' : 'header__bottom-menu-link ');
		$class.=($depth>=1&&$item->current ? ' active ':'');
		$class.=' d-'.$depth;
		$addclass=' ';
		foreach ($item->classes as $key => $val) {
			$addclass.=' '.$val;
		}
		$class.=$addclass;
		$class.=($args->walker->has_children)?' smenu_select':null;
		$output.= '<li class="'.$class.'">';
		$jsbtn=(in_array('headmidmenu_item-logo', $item->classes)) ? ' headmidmenu_link-logo' : '';
		$actlnk=($item->current)? ' active':null;
		// print_r($item->classes);
		$attributes  = ($depth>=1) ? ' class="mmenu_link"' : ' class="header_blink'.$jsbtn.$actlnk.'"';
		if( !empty ( $item->attr_title ) && $item->attr_title !== $item->title ){
			// Avoid redundant titles
			$attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
		} else {
			$title	   = apply_filters( 'the_title', $item->title, $item->ID );
			$attributes .= ' title="' . $title .'"';
		}
		! empty ( $item->url )
			and $attributes .= ' href="' . esc_attr( $item->url ) .'"';
		$attributes  = trim( $attributes );
		$title	   = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";
		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters('walker_nav_menu_start_el',   $item_output,   $item,   $depth,   $args);
	}
	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		$output.='<ul class="submenu">';
	}
	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ){
		$output .= '</ul>';
	}
	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '</li>';
	}
}

function function_page1(){
    add_menu_page( 'custom menu title', 'Главная Страница', 'manage_options', 'post.php?post=164&action=edit&lang=en', '', 'dashicons-welcome-write-blog', 1  );
}
add_action('admin_menu','function_page1');

function function_page2(){
    add_menu_page( 'custom menu title', 'Галерея', 'manage_options', 'post.php?post=246&action=edit&lang=en', '', 'dashicons-welcome-write-blog', 1  );
}
add_action('admin_menu','function_page2');

add_filter( 'the_seo_framework_description_output', 'my_special_description', 10, 2 );
add_filter( 'the_seo_framework_ogdescription_output', 'my_special_description', 10, 2 );
add_filter( 'the_seo_framework_twitterdescription_output', 'my_special_description', 10, 2 );
/**
 * Alters the description on special conditions.
 *
 * @param string $description The current description.
 * @param int $id The Post, Page or Term ID.
 * @return string Description. Does not need to be escaped.
 */
function my_special_description( $description = '', $id = 0 ) {

    /**
     * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
     */
    if ( is_post_type_archive( 'news' ) ) {
        $description = '🌟 Новости Академии Детства ➳ что нового в частном детском саду в Одессе. ➳ Последние события
и детские мероприятия на сайте детсада. ➳ Читать новости онлайн ➳ будьте вкурсе успехов Ваших детей 🌟';
    }
	else if ( is_post_type_archive( 'notice' )){
		$description = 'Статьи, заметки, наблюдения, советы для родителей и детей в детском саду. Академия Детства постарается ответить на вопросы воспитания в детском саду';
	}

    return $description;
}

add_filter( 'the_seo_framework_the_archive_title', 'my_archive_title', 10, 2 );
function my_archive_title( $title, $term ) {
    if ( is_post_type_archive( 'news' )){
		return $title = 'Новости: частный детский садик. Детский сад на Таирово';
	}
	else if ( is_post_type_archive( 'notice' )){
		return $title = 'Заметки для родителей - Советы родителям от Академии Детства';
	}
}


function popup_scripts() {
	wp_enqueue_style( 'featherlight.css', get_stylesheet_directory_uri() . '/css/featherlight.min.css' );
	wp_enqueue_script( 'featherlight.js', get_stylesheet_directory_uri() . '/js/featherlight.min.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'popup_scripts' );



function is_post_type($type)
{
	global $post;
	if($type == get_post_type(get_queried_object_id())) return true;
	return false;
}


add_filter( 'the_seo_framework_rel_canonical_output', function()
{
	if(is_post_type('news') || is_post_type('notice'))
	{
		global $wp;
		$url = home_url($wp->request);
		if(strpos($url, '/page')) return substr($url, 0, (strpos($url, '/page')+1));
		else return $url.'/';
	}
});


function my_comments_callback( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;


    ?>


    <li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="li-comment-<?php comment_ID(); ?>">
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
			<?php
				if ( $args['avatar_size'] != 0 ) {
					echo '<a class="comment-avatar">';
						echo get_avatar( $comment, $args['avatar_size'] );
					echo '</a>';
				}
	    	?>

			<h4 class="comment-subject"><?php echo get_comment_author_link(); ?></h4>
            <?php comment_text(); ?>

            <?php if ( $comment->comment_approved == '0' ) : ?>
			    <em class="comment-awaiting-moderation"><?php _e( 'Ваш комментарий будет добавлен после проверки модератором.', 'lotus' ) ?></em>
			    <br />
			<?php endif; ?>

			<span class="comment-meta">
                <?php printf( __( '%1$s %2$s', 'lotus' ), get_comment_time(), get_comment_date() ) ?>
            </span>

            <div class="action">
                <?php if ( current_user_can( 'edit_comment', $comment->comment_ID ) ) { ?>
			    	<?php edit_comment_link( __( 'Редактировать', 'lotus' ), '  ', '' ); ?>
			    <?php } ?>
			    	<?php comment_reply_link( array_merge( $args, array( 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ) ?>
            </div>
        </div>
    <?php
}

function wpb_move_comment_field_to_bottom( $fields )
{
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );